#!make

all: k8 kafka

include .devcontainer-base/Makefile
include ${FEATURE_PROJECT}/k8.mk
include ${FEATURE_PROJECT}/kafka.mk

.PHONY: k8
k8:
	@$(MAKE) -C 1.k8 all

.PHONY: kafka
kafka:
	@$(MAKE) -C 2.kafka all

.PHONY: kubefwd
kubefwd: .kafka-wait-ready
kubefwd:
	@$(MAKE) -C 3.kubefwd all

.PHONY: stop
stop:
	@$(MAKE) -C 3.kubefwd stop && sleep 1 || true
	@# nothing to do for kafka helm installation
	@$(MAKE) -C 1.k8 stop

.PHONY: delete
delete:
	@$(MAKE) -C 3.kubefwd stop || true
	@sleep 1
	@$(MAKE) -C 2.kafka delete || true
	@sleep 3
	@pidof k9s 1>/dev/null 2>/dev/null && kill $(shell pidof k9s) || true
	@$(MAKE) -C 1.k8 delete

.PHONY: k9s
k9s: .k8-wait-conn
k9s:
	@k9s --headless --readonly

.PHONY: tests
tests: .kafka-kubefwd-wait-ready
tests:
	@echo "> Run tests for a few seconds (produce, consume)"
	$(MAKE) -C 9.tests-kafka/ topics-create
	NUM_RECORDS=100000 \
	THROUGHPUT=5000 \
		$(MAKE) -C 9.tests-kafka/ producer-perf-test
	$(MAKE) -C 9.tests-kafka/ consumer-perf-test
	@echo "> Test completed."
