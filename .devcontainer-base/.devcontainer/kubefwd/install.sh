#!/bin/sh

set -e

URL="https://github.com/txn2/kubefwd/releases/download/${VERSION}/kubefwd_Linux_x86_64.tar.gz"
TMP_DIR=$(mktemp -d)
TARGET="$TMP_DIR/kubefwd.tar.gz"
BIN="$TMP_DIR/kubefwd"

if ! command -v kubefwd &>/dev/null; then
    curl -fsSL -o $TARGET "$URL"
    tar -C $TMP_DIR -zxf $TARGET
    install -m a+rx -t /usr/local/bin $BIN
    rm -rf $TMP_DIR
fi

# completion
kubefwd completion bash > /etc/bash_completion.d/kubefwd
