#!/bin/sh

set -e

if command -v apk; then
    echo "Not implemented at the moment".
    exit 1
elif command -v apt; then
    apt-get update
    apt-get install -y openjdk-11-jdk
    rm -rf /var/lib/apt/lists/*
fi
