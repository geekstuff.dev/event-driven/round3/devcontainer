#!make

all:
	@echo ">>> Ensure local Kubernetes cluster"
	@$(MAKE) ensure

.PHONY: ensure
ensure:
	@$(MAKE) exists 2>/dev/null || $(MAKE) create
	@$(MAKE) info

.PHONY: create
create:
	@kind create cluster --name ${CODENAME} --wait 5m --config kind-config.yaml
	@$(MAKE) kubeconfig

.PHONY: kubeconfig
kubeconfig:
	@kind get kubeconfig --name ${CODENAME} --internal > ${KUBECONFIG}

.PHONY: delete
delete:
	@#echo -n "Are you sure? [y/N] " && read answer && test "$${answer:-N}" = "y"
	@kind delete cluster --name ${CODENAME}

.PHONY: clusters
clusters:
	@kind get clusters --quiet

.PHONY: exists
exists:
	@test $(shell $(MAKE) clusters | wc -l) -ne 0

.PHONY: info
info:
	@kubectl cluster-info | grep 'https'
	@echo -e "\033[0;32mK8 control plate\033[0m from dev host\033[0;33m Use vscode ports tab \033[0m"

.PHONY: cluster-cacert
cluster-cacert:
	@kubectl config view --raw -o go-template='{{index ((index (index .clusters 0) "cluster")) "certificate-authority-data"|base64decode}}'
