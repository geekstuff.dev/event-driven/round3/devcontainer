#!/bin/sh

set -e

URL="https://kind.sigs.k8s.io/dl/v${VERSION}/kind-linux-amd64"
TMP_DIR=$(mktemp -d)
BIN="$TMP_DIR/kind"

if ! command -v kind &>/dev/null; then
    curl -fsSL -o "$BIN" "$URL"
    install -m 755 -t /usr/local/bin "$BIN"
fi

# completion
kind completion bash > /etc/bash_completion.d/kind

#
mkdir -p /home/dev/.kind
chown -R dev:dev /home/dev/.kind

