#!/bin/sh

set -e

URL="https://github.com/k3d-io/k3d/releases/download/v${VERSION}/k3d-linux-amd64"
TMP_DIR=$(mktemp -d)
BIN="$TMP_DIR/k3d"

if ! command -v k3d &>/dev/null; then
    curl -fsSL -o "$BIN" "$URL"
    install -m 755 -t /usr/local/bin "$BIN"
fi

# completion
k3d completion bash > /etc/bash_completion.d/k3d

# ensure home folder
mkdir -p /home/dev/.k3d
chown -R dev:dev /home/dev/.k3d
