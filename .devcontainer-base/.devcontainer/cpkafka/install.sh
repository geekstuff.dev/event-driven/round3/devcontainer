#!/bin/sh

set -e

# Add confluent helm repo
sudo -u dev helm repo add confluentinc https://confluentinc.github.io/cp-helm-charts/
sudo -u dev helm repo update

# ensure lib folder
mkdir -p $FEATURE_CPKAFKA
chown -R root:root $FEATURE_CPKAFKA
chmod 755 $FEATURE_CPKAFKA

# prep fetching confluent platform
ARCHIVE_VERSION=7.3
VERSION=7.3.1
URL=https://packages.confluent.io/archive/${ARCHIVE_VERSION}/confluent-${VERSION}.tar.gz
TMP_DIR=$(mktemp -d)
CP_DIR=${TMP_DIR}/confluent-platform
TAR=${TMP_DIR}/software.tar.gz

# fetch and uncompress
echo "Fetch confluent platform "
mkdir -p $CP_DIR
curl -fsSL -o ${TAR} ${URL}
tar -C ${CP_DIR} --strip-components=1 -zxf ${TAR}
rm -f ${TAR}

# Remove large and unnecessary and leave hint at what it was
rm -rf ${CP_DIR}/libexec
mkdir -p ${CP_DIR}/libexec/linux_amd64
touch ${CP_DIR}/libexec/linux_amd64/confluent

# Move files
mv ${CP_DIR} ${FEATURE_CPKAFKA}/
CP_DIR=${FEATURE_CPKAFKA}/confluent-platform

# Add logs folder
mkdir ${CP_DIR}/logs

# Cleanup
rm -rf ${TMP_DIR}

# Ensure permissions
chown -R dev:dev ${FEATURE_CPKAFKA}
chmod -R o-rwx ${FEATURE_CPKAFKA}

# copy extra files from kafka
mkdir -p /etc/kafka
cp $CP_DIR/etc/kafka/tools-log4j.properties /etc/kafka/

chmod go+x /etc/kafka
chmod -R go+r /etc/kafka
