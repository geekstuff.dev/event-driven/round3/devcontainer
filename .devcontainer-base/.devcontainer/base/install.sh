#!/bin/sh

set -e

if command -v apk; then
    # Fix that was and should go back to basics feature
    if getent group ping; then
        groupdel ping
    fi

    if ! getent group 999; then
        # create 999 docker group
        groupadd -g 999 docker2
        # make user part of both
        usermod -aG docker2 dev
    fi

    # Add some packages (missing netcat)
    apk add --update --no-cache iputils pv
elif command -v apt; then
    # Add some packages
    apt-get update
    apt-get install -y iputils-ping pv netcat-openbsd
    rm -rf /var/lib/apt/lists/*
fi

# k9s completion (TODO move to feature)
k9s completion bash > /etc/bash_completion.d/k9s
