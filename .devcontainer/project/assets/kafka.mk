#!make

.PHONY: .kafka-wait-ready
.kafka-wait-ready: REQUEST_TIMEOUT ?= 3s
.kafka-wait-ready: SLEEP ?= 3
.kafka-wait-ready:
	@echo "> waiting until kafka is ready"
	@while true; do \
		$(MAKE) .kafka-check 2>/dev/null \
		&& sleep 1 \
		&& break \
		|| { printf ". "; sleep ${SLEEP}; }; \
	done

.PHONY: .kafka-kubefwd-wait-ready
.kafka-kubefwd-wait-ready: SLEEP ?= 3
.kafka-kubefwd-wait-ready: BROKERS := $(shell echo ${BROKERS} | cut -d',' -f1)
.kafka-kubefwd-wait-ready: BROKER := $(shell echo ${BROKERS} | cut -d':' -f1)
.kafka-kubefwd-wait-ready: PORT := $(shell echo ${BROKERS} | cut -d':' -f2)
.kafka-kubefwd-wait-ready:
	@echo "> waiting until kubefwd -> broker is ready"
	@while true; do \
		nc -w ${SLEEP} -vz ${BROKER} ${PORT} 1>/dev/null 2>/dev/null \
		&& sleep 1 \
		&& break \
		|| { printf ". "; sleep ${SLEEP}; }; \
	done

.PHONY: .kafka-check
.kafka-check:
	@test "$(shell $(MAKE) .kafka-status)" = "True True True"

.PHONY: .kafka-status
.kafka-status:
	@kubectl get pods \
			-n cpkafka \
			-o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}' \
			2>/dev/null
