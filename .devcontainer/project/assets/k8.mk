#!make

.PHONY: .k8-wait-conn
.k8-wait-conn: REQUEST_TIMEOUT ?= 3s
.k8-wait-conn: SLEEP ?= 3
.k8-wait-conn:
	@echo "> waiting until kubernetes is ready"
	@while true; do \
		kubectl --request-timeout=${REQUEST_TIMEOUT} get nodes 1>/dev/null 2>/dev/null && break \
			|| { printf ". "; sleep ${SLEEP}; }; \
	done
