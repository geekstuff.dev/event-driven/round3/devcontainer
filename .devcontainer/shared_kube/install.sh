#!/bin/sh

set -e

mkdir -p /home/dev/.shared_kube
chown dev:dev /home/dev/.shared_kube
chmod go-rwx /home/dev/.shared_kube
